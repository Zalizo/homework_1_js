/*
1. Функції дозволяють розбивати складні завдання на більш дрібні і прості, що координально знижує загальну складність поставленої задачі.
2. Будь яка функція це об'єкт, а об'єкт можна використовувати у своїх цілях, спосіб оголосити зміну, викликати зміну, прив'язати зміну.
3. Оператор return завершує виконання поточної функції та повертає її значення.
*/
let num1;
do {
  num1 = parseFloat(prompt("Enter the first number:"));
} while (isNaN(num1));

let num2;
do {
  num2 = parseFloat(prompt("Enter the second number:"));
} while (isNaN(num2));

let operator;
do {
  operator = prompt("Enter the operator (+, -, *, /):");
} while (
  operator !== "+" &&
  operator !== "-" &&
  operator !== "*" &&
  operator !== "/"
);

const result = calculate(num1, num2, operator);
console.log(result);

function calculate(num1, num2, operator) {
  let result;
  switch (operator) {
    case "+":
      result = num1 + num2;
      break;
    case "-":
      result = num1 - num2;
      break;
    case "*":
      result = num1 * num2;
      break;
    case "/":
      result = num1 / num2;
      break;
    default:
      result = "Invalid operator";
  }
  return result;
}
