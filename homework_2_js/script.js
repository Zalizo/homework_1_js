/* 
1. Існує 6 типів данних: undefined, null, boolean, string, number i object.
2. == використовується для порівняння двох змінних, а === використовується для порівняння двох змінних по типу даних (typeof).
3. Оператор - це символ, який повинен виконати операцію з операндами (+, -, *, /, %)
*/

let name = prompt("Введіть своє ім'я:");
let age = prompt("Введіть свій вік:");

while (!name || isNaN(age)) {
  alert("Ви ввели некоректні дані, спробуйте ще раз.");
  name = prompt("Введіть своє ім'я:");
  age = prompt("Введіть свій вік:");
}
age = Number(age);

if (age < 18) {
  alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
  let confirmed = confirm("Are you sure you want to continue?");
  if (confirmed) {
    alert("Welcome, " + name + "!");
  } else {
    alert("You are not allowed to visit this website.");
  }
} else {
  alert("Welcome, " + name + "!");
}
