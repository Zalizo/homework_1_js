/* 
1.Цикл це спосіб повторити один і той же код кілька разів.
2. while - доки умова є вірною, виконується код із тіла циклу.
   do while - спочатку виконує тіло, а потім перевіряє умову.
   for - спочатку один раз виконується початок, потім при кожній ітерації: перевіряється умова, виконується тіло циклу
3. Явне перетворення - коли розробник хоче навмисне зробити перетворення типів ( наприклад Number(value)).
   Не явне перетворення - перетворення між різними типами може відбуватися автоматично (1 == null, "20" - "1")
   */

function isPrime(num) {
  for (let i = 2; i < num; i++) {
    if (num % i === 0) {
      return false;
    }
  }
  return num !== 1;
}

function findMultiplesOf5(num) {
  if (!Number.isInteger(num)) {
    alert("Please enter an integer number");
    num = parseInt(prompt("Enter a number:"));
    findMultiplesOf5(num);
  } else {
    let multiples = [];
    for (let i = 0; i <= num; i++) {
      if (i % 5 === 0) {
        multiples.push(i);
      }
    }
    if (multiples.length === 0) {
      console.log("Sorry, no numbers");
    } else {
      console.log(multiples);
    }
  }
}

function findPrimesInRange(m, n) {
  if (!Number.isInteger(m) || !Number.isInteger(n)) {
    alert("Please enter integer numbers");
    m = parseInt(prompt("Enter the first number:"));
    n = parseInt(prompt("Enter the second number:"));
    findPrimesInRange(m, n);
  } else {
    let min = Math.min(m, n);
    let max = Math.max(m, n);
    let primes = [];
    for (let i = min; i <= max; i++) {
      if (isPrime(i)) {
        primes.push(i);
      }
    }
    console.log(primes);
  }
}

let num = parseInt(prompt("Enter a number:"));
findMultiplesOf5(num);
let m = parseInt(prompt("Enter the first number:"));
let n = parseInt(prompt("Enter the second number:"));
findPrimesInRange(m, n);
